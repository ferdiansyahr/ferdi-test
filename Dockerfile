# Define base image
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env

# Copy project files
WORKDIR /source
COPY ["src/ferdi-test/ferdi-test.csproj", "./ferdi-test/ferdi-test.csproj"]

# Restore
RUN dotnet restore "./ferdi-test/ferdi-test.csproj"

# Copy all source code
COPY . .

# Publish
WORKDIR /source/src
RUN dotnet publish -c Release -o /publish

